UTCSTAMP
========

UTCSTAMP is an EDNS extension to allow a client to request from a
server that it includes a timestamp in the response message, and for
the server to provide it, if requested and deemed appropriate.  This
is primarily intended as a debugging tool.

The "draft-liman-dns-utcstamp" directory contains an Internet Draft that
describes the extension.

In the "bind" directory you will find crude patches to BIND 9.13.3,
9.13.4, and 9.17.15 that implement this extension. To apply the
patched, pick up the development code for BIND from ISC, and the
corresponding patch file from here and ...

**NOTE!** The patch file for 9.13.4 is broken and should not be used.
Contact me if you *really* need that file, and I'll generate a new one
for you. But it's really water under the bridges.

```
tar Jxf bind-9.17.15.tar.xz
cd bind-9.17.15
patch -p0 < ../bind-9.17.15-utcstamp.patch
./configure ...
make
```

Time-stamp: "2021-07-05 14:46:39 UTC liman"

<!---
;; Local Variables:
;; eval: (setq time-stamp-line-limit -30)
;; eval: (setq time-stamp-time-zone "UTC")
;; eval: (setq time-stamp-format "%:y-%02m-%02d %02H:%02M:%02S %Z %u")
;; End:
-->
